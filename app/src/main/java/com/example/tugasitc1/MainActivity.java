package com.example.tugasitc1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_nama,et_email,et_pw,et_major,et_nim,et_motiv;
    Button btn_reset,btn_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_nama = findViewById(R.id.Et_nama);
        et_email = findViewById(R.id.et_email);
        et_major = findViewById(R.id.et_major);
        et_nim = findViewById(R.id.et_nim);
        et_motiv = findViewById(R.id.et_motiv);
        et_pw = findViewById(R.id.et_pw);

        btn_reset = findViewById(R.id.btn_reset);
        btn_signup = findViewById(R.id.btn_signup);

        btn_reset.setOnClickListener(this);
        btn_signup.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        String nama = et_nama.getText().toString();
        String email = et_email.getText().toString();
        String pw = et_pw.getText().toString();
        String major = et_major.getText().toString();
        String motiv = et_motiv.getText().toString();
        String nim = et_nim.getText().toString();


        switch (v.getId())
        {
            case R.id.btn_reset:
                et_nama.setText("");
                et_email.setText("");
                et_pw.setText("");
                et_major.setText("");
                et_nim.setText("");
                et_motiv.setText("");

            break;
            case R.id.btn_signup:
                if (nama.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "nama kosong", Toast.LENGTH_SHORT).show();
                }

                else if (email.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "email kosong", Toast.LENGTH_SHORT).show();
                }
                else if (pw.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "pw kosong", Toast.LENGTH_SHORT).show();
                }
                else if (major.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "major kosong", Toast.LENGTH_SHORT).show();
                }
                else if (nim.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "nim kosong", Toast.LENGTH_SHORT).show();
                }
                else if (motiv.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "motivation kosong", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(getApplicationContext(), "sign up succses", Toast.LENGTH_SHORT).show();
                }

                break;


        }


    }


}
